#gasto en vivienda
library(readr)
library(stringr)
library(dplyr)
#library(xlsx)
rhogares<-"C:/Users/IEVIRF01/Documents/BD/ENIGH/2018/enigh2018_ns_hogares_csv/hogares.csv"
rpoblacion<-"C:/Users/IEVIRF01/Documents/BD/ENIGH/2018/enigh2018_ns_poblacion_csv/poblacion.csv"
rconcentrado<-"C:/Users/IEVIRF01/Documents/BD/ENIGH/2018/enigh2018_ns_concentradohogar_csv/concentradohogar.csv"
rtrabajos<-"C:/Users/IEVIRF01/Documents/BD/ENIGH/2018/enigh2018_ns_trabajos_csv/trabajos.csv"
ringresos<-"C:/Users/IEVIRF01/Documents/BD/ENIGH/2018/enigh2018_ns_ingresos_csv/ingresos.csv"
rgastos<-"C:/Users/IEVIRF01/Documents/BD/ENIGH/2018/enigh2018_ns_gastoshogar_csv/gastoshogar.csv"
rerogaciones<-"C:/Users/IEVIRF01/Documents/BD/ENIGH/2018/enigh2018_ns_erogaciones_csv/erogaciones.csv"
rvivienda<-"C:/Users/IEVIRF01/Documents/BD/ENIGH/2018/enigh2018_ns_viviendas_csv/viviendas.csv"
# #
directorio_trabajo<-"F:/NuevosR/enigh"
#
# rhogares<-"/home/francisco/Documentos/Infonavit/BD/ENIGH/2018/hogares.csv"
# rpoblacion<-"/home/francisco/Documentos/Infonavit/BD/ENIGH/2018/poblacion.csv"
# rconcentrado<-"/home/francisco/Documentos/Infonavit/BD/ENIGH/2018/concentradohogar.csv"
# rtrabajos<-"/home/francisco/Documentos/Infonavit/BD/ENIGH/2018/trabajos.csv"
# ringresos<-"/home/francisco/Documentos/Infonavit/BD/ENIGH/2018/ingresos.csv"
# rgastos<-"/home/francisco/Documentos/Infonavit/BD/ENIGH/2018/gastoshogar.csv"
# rerogaciones<-"/home/francisco/Documentos/Infonavit/BD/ENIGH/2018/erogaciones.csv"
# rvivienda<-"/home/francisco/Documentos/Infonavit/BD/ENIGH/2018/viviendas.csv"
#
# directorio_trabajo<-"/home/francisco/Documentos/git/enigh/"
#####
especificaciones<-"libreria/spec_ENIGH2018.R"
funciones<-"libreria/deciles.R"
#
setwd(directorio_trabajo)
source(especificaciones)
source(funciones)
#
gastos <- read_csv(rgastos, locale = locale(encoding = "ISO-8859-1"),  col_names = TRUE, col_types = do.call(cols_only, spec_gasto))
#gastos<-read.csv(rgastos,colClasses=c('character'),na.strings=c(""," ","NA"))
#colnames(gastos)[1] <- "folioviv"
#
#gastos$gasto_tri<-as.numeric(gastos$gasto_tri)
gastos1<-select(gastos,folioviv,foliohog,clave,gasto_tri)
rm(gastos)               
alquiler1<-subset(gastos1,clave=='G101')
alquiler1a<-setNames(aggregate(list(alquiler1$gasto_tri), by = list(alquiler1$folioviv,alquiler1$foliohog), sum,na.rm = T),c('folioviv','foliohog','alquiler1'))
US1<-subset(gastos1,clave=='R008' |clave=='R009'|clave=='R010'|clave=='R011')
US1a<-setNames(aggregate(list(US1$gasto_tri), by = list(US1$folioviv,US1$foliohog), sum,na.rm = T),c('folioviv','foliohog','GUS'))
alquiler2<-subset(gastos1,clave=='G004')
alquiler2a<-setNames(aggregate(list(alquiler2$gasto_tri), by = list(alquiler2$folioviv,alquiler2$foliohog), sum,na.rm = T),c('folioviv','foliohog','alquiler2'))
mantenimiento<-subset(gastos1,clave=='K038'|clave=='K039')
Smant<-setNames(aggregate(list(mantenimiento$gasto_tri), by = list(mantenimiento$folioviv,mantenimiento$foliohog), sum,na.rm = T),c('folioviv','foliohog','mantenimiento'))
#rm(gastos)
#
# hogares<-read.csv(rhogares,colClasses=c('character'),na.strings=c(""," ","NA"))
# poblacion<-read.csv(rpoblacion,colClasses=c('character'),na.strings=c(""," ","NA"))
# concentrado<-read.csv(rconcentrado,colClasses=c('character'),na.strings=c(""," ","NA"))
# trabajos<-read.csv(rtrabajos,colClasses=c('character'),na.strings=c(""," ","NA"))
# #ingresos<-read.csv(ringresos,colClasses=c('character'),na.strings=c(""," ","NA"))
# #gastos<-read.csv(rgastos,colClasses=c('character'),na.strings=c(""," ","NA"))
# erogaciones<-read.csv(rerogaciones,colClasses=c('character'),na.strings=c(""," ","NA"))
# vivienda<-read.csv(rvivienda,colClasses=c('character'),na.strings=c(""," ","NA"))
#
hogares <- read_csv(rhogares, locale = locale(encoding = "ISO-8859-1"),  col_names = TRUE, col_types = do.call(cols_only, spec_hogares))
poblacion <- read_csv(rpoblacion, locale = locale(encoding = "ISO-8859-1"),  col_names = TRUE, col_types = do.call(cols_only, spec_poblacion))
concentrado <- read_csv(rconcentrado, locale = locale(encoding = "ISO-8859-1"),  col_names = TRUE, col_types = do.call(cols_only, spec_concentrado))
trabajos <- read_csv(rtrabajos, locale = locale(encoding = "ISO-8859-1"),  col_names = TRUE, col_types = do.call(cols_only, spec_trabajos))
erogaciones <- read_csv(rerogaciones, locale = locale(encoding = "ISO-8859-1"),  col_names = TRUE, col_types = do.call(cols_only, spec_erogaciones))
vivienda <- read_csv(rvivienda, locale = locale(encoding = "ISO-8859-1"),  col_names = TRUE, col_types = do.call(cols_only, spec_vivienda))
#
# colnames(concentrado)[1] <- "folioviv"
# colnames(hogares)[1] <- "folioviv"
# colnames(vivienda)[1] <- "folioviv"
# colnames(poblacion)[1] <- "folioviv"
# colnames(trabajos)[1] <- "folioviv"
# colnames(erogaciones)[1] <- "folioviv"
# #colnames(gastos)[1] <- "folioviv"
#
# concentrado$factor<-as.numeric(concentrado$factor)
# concentrado$ocupados<-as.numeric(concentrado$ocupados)
# concentrado$tot_integ<-as.numeric(concentrado$tot_integ)
# vivienda$renta<-as.numeric(vivienda$renta)
# vivienda$factor<-as.numeric(vivienda$factor)
# concentrado$alquiler<-as.numeric(concentrado$alquiler)
# concentrado$tot_integ<-as.numeric(concentrado$tot_integ)
# concentrado$energia<-as.numeric(concentrado$energia)
# concentrado$pred_cons<-as.numeric(concentrado$pred_cons)
# concentrado$ing_cor<-as.numeric(concentrado$ing_cor)
#####
hogarest<-sum(concentrado$factor)
hogaresto<-sum(concentrado$factor[concentrado$ocupados!=0])
integht<-sum(concentrado$tot_integ*concentrado$factor)
#
#Gastos directos
#G101: Alquiler de vivienda
#
concentrado1<-merge(concentrado,alquiler1a,by=c('folioviv','foliohog'),all.x = TRUE)
#G004 Alquiler de terrenos
concentrado1<-merge(concentrado1,alquiler2a,by=c('folioviv','foliohog'),all.x = TRUE)
#US
concentrado1<-merge(concentrado1,US1a, by=c('folioviv','foliohog'),all.x = TRUE)
#Mantenimiento
concentrado1<-merge(concentrado1,Smant, by=c('folioviv','foliohog'),all.x = TRUE)

# predio
#Pred_cons
# Agua
#agua
# Energ?a
#energia
# hipoteca
hi009<-subset(erogaciones,clave=='Q009')
#hi010<-subset(erogaciones,clave=='Q010')
hi011<-subset(erogaciones,clave=='Q011')
#hi<-subset(erogaciones,clave=='Q011')
hi1<-select(hi009,folioviv,foliohog,clave,ero_tri)
#hi2<-select(hi010,folioviv,foliohog,clave,ero_tri)
hi3<-select(hi011,folioviv,foliohog,clave,ero_tri)
hi01<-setNames(aggregate(list(hi1$ero_tri), by = list(hi1$folioviv,hi1$foliohog), sum,na.rm=TRUE),c('folioviv','foliohog','Ghipoteca1'))
#hi02<-setNames(aggregate(list(hi2$ero_tri), by = list(hi2$folioviv,hi2$foliohog), sum,na.rm=TRUE),c('folioviv','foliohog','Ghipoteca2'))
hi03<-setNames(aggregate(list(hi3$ero_tri), by = list(hi3$folioviv,hi3$foliohog), sum,na.rm=TRUE),c('folioviv','foliohog','Ghipoteca3'))
concentrado1<-merge(concentrado1,hi01, by=c('folioviv','foliohog'), all.x = TRUE)
#concentrado1<-merge(concentrado1,hi02, by=c('folioviv','foliohog'), all.x = TRUE)
concentrado1<-merge(concentrado1,hi03, by=c('folioviv','foliohog'), all.x = TRUE)
#US
#gasto en hogar
#concentrado1$GH<-rowSums(concentrado1[,c('Galquiler','Alquiler2','GUS','Ghipoteca1','Ghipoteca2','Ghipoteca3','energia','pred_cons')],na.rm = T)
#concentrado1$GH<-rowSums(concentrado1[,c('alquiler1','alquiler2','GUS','Ghipoteca2','Ghipoteca3','energia','pred_cons')],na.rm = T)
#concentrado1$GH1<-rowSums(concentrado1[,c('alquiler','GUS','Ghipoteca2','Ghipoteca3','energia','pred_cons')],na.rm = T)
#alquiler1=G101 casa
#Alquiler2=G004 terreno
#GUS=Gasto en servicios
#Ghipoteca3=Pago de hipotecas de bienes inmuebles: casas, locales, terrenos, edificios, etc
concentrado1$GH<-rowSums(concentrado1[,c('alquiler1','alquiler2','GUS','mantenimiento','Ghipoteca3','energia','pred_cons')],na.rm = T)

#
concentrado1$GHf<-concentrado1$GH*concentrado1$factor
concentrado1$inGH<-concentrado1$GH/concentrado1$ing_cor
#
concentrado1<-deciles(concentrado1,"factor","ing_cor","folioviv","foliohog")
#concentrado1<-merge(concentrado1,decil,by=c(1,2))
#comparacion<-setdiff(c(decil$folioviv,decil$foliohog), c(concentrado1$folioviv,concentrado1$foliohog))

#concentrado1 = mutate(concentrado1, indice = ntile(concentrado1$ing_cor,10))
#
cuantil<-'decil'
###   

Cuadro0<-setNames(aggregate(list(concentrado1$factor), by = list(concentrado1[[cuantil]]), sum, na.rm=TRUE, na.action=NULL),c('cuantil','Numero de hogares'))
#  
Cuadro00<-setNames(aggregate(list(concentrado1$GHf), by = list(concentrado1[[cuantil]]), sum, na.rm=TRUE, na.action=NULL),c('cuantil','GH'))
Cuadro00$GH<-Cuadro00$GH/3
Cuadro00$GHR<-Cuadro00$GH/97.32*100
#
Cuadro00<-merge(Cuadro0,Cuadro00,by=1)
Cuadro00$GHRP<-Cuadro00$GHR/Cuadro00$`Numero de hogares`
#############################################################
Cuadro00a<-setNames(aggregate(list(concentrado1$ing_cor*concentrado1$factor), by = list(concentrado1[[cuantil]]), sum, na.rm=TRUE, na.action=NULL),c('cuantil','GC'))
Cuadro00a$GC<-Cuadro00a$GC/3
Cuadro00a$GCR<-Cuadro00a$GC/97.32*100
#
Cuadro00a<-merge(Cuadro0,Cuadro00a,by=1)
Cuadro00a$GCRP<-Cuadro00a$GCR/Cuadro00a$`Numero de hogares`
#
#
Cuadro00i<-setNames(aggregate(list(concentrado1$Ghipoteca1*concentrado1$factor), by = list(concentrado1[[cuantil]]), sum, na.rm=TRUE, na.action=NULL),c('cuantil','GC'))
Cuadro00i$GC<-Cuadro00i$GC/3
Cuadro00i$GCR<-Cuadro00i$GC/97.32*100
#
Cuadro00j<-setNames(aggregate(list(concentrado1$vivienda), by = list(concentrado1[[cuantil]]), mean, na.rm=TRUE, na.action=NULL),c('cuantil','GV'))
Cuadro00j$GV<-Cuadro00j$GV/3
Cuadro00j$GVR<-Cuadro00j$GV/97.32*100
resultado1<-"C:/Users/IEVIRF01/Documents/Resultados/ENIGH/gastohog2018.csv"
resultado2<-"C:/Users/IEVIRF01/Documents/Resultados/ENIGH/gastohog2018v02.csv"
resultado3<-"C:/Users/IEVIRF01/Documents/Resultados/ENIGH/gastohog2018b.csv"
write.csv(concentrado1,resultado1,row.names = FALSE)
write.csv(Cuadro00,resultado2,row.names = FALSE)
write.csv(Cuadro00i,resultado3,row.names = FALSE)


