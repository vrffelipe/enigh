En este repositorio se guardará la programación para replicar los datos de análisis de la ENIGH que publicó el INEGI para el año 2018.

Archivos:

ENIGH2018.-Avance para replicar el la presentación del INEGI

ENIGH2018_ttenencia.-Tenencia de la vivienda por situación laboral y por deciles.

ENIGH2018_gastoviviendaingreso.-Gasto en vivienda respecto al ingreso

ENIGH2018_gastoV01.-Proporción del gasto en vivienda respecto al gasto total a nivel nacional

ENIGH2018_gastoV01_pruebas.-Pruebas del script anterior

ENIGH2018_gastoEstadosMapaV01.-Proporción del gasto en vivienda respecto al gasto total a nivel estado y con un mapa.

ENIGH2018_forinfV01.-Formales e informales a nivel nacional

ENIGH2018_forinfV01_alcance.-Formales e informales a nivel Estado

ENIGH208_transinf.-Proporción de las transferencias a los trabajadores informales


